import { NgModule } from '@angular/core';
import { SignaturePad } from './angular-signaturepad.component';

@NgModule({
  declarations: [SignaturePad],
  imports: [],
  exports: [SignaturePad],
})
export class SignaturePadModule {}
